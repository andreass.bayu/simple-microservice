package main

import (
	"simple-microservice/register-module/db"
	"simple-microservice/register-module/route"
)

func main() {
	db.Init()
	e := route.Init()

	e.Logger.Fatal(e.Start(":1232"))
}
