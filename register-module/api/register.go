package api

import (
	"net/http"
	"simple-microservice/register-module/db"
	"simple-microservice/register-module/model"

	"github.com/labstack/echo"
)

func PostRegister(c echo.Context) error {

	name := c.FormValue("name")
	username := c.FormValue("username")
	password := c.FormValue("password")

	db := db.DbManager()
	user := model.User{}
	var count int64
	// db.Find(&user, "name = ?", username).Count(&count)
	db.Model(&user).Where("name = ?", username).Count(&count)

	// Throws already exist error
	if count > 0 {
		return c.JSON(http.StatusOK, map[string]string{
			"message": "Username sudah ada",
		})
	}

	hashedpassword, _ := HashPassword(password)

	newUser := model.User{Name: name, Username: username, Password: hashedpassword}

	db.Create(&newUser)

	return c.JSON(http.StatusOK, map[string]string{
		"message": "Ok",
	})

}
