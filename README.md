# Simple Microservice Using Go

## Base application
1.  `auth-server` -> Authentication server using Go
2.  `frontend-user` -> Simple User Interface with Html + CSS + JS
3.  `register-module` -> Register API using Go

## Architecture
<img src="scheme.png" width="500">
