package main

import (
	"simple-microservice/auth-server/db"
	"simple-microservice/auth-server/route"
)

func main() {
	db.Init()
	e := route.Init()

	e.Logger.Fatal(e.Start(":1323"))
}
